// let proxyObj = {}

// proxyObj['/'] = {
//   ws: false,
//   target: 'http://localhost:8081',
//   changeOrigin: true,
//   pathRewrite: {
//     '^/': '/'
//   }
// }
// module.exports = {
//   devServer: {
//     host: 'localhost',
//     port: 8080,
//     proxy: proxyObj
//   }
// }

module.exports = {
  devServer: {
    port: 8080,
    open: true,
    proxy: {
      '/': {
        ws: true,
        target: 'http://localhost:8081/',
        changeOrigin: true,
        pathRewrite: {
          '^/': ''
        }
      },
      '/ws': {
        ws: true,
        target: 'ws://localhost:8081/',
        changeOrigin: true,
        pathRewrite: {
          '^/': ''
        }
      }
    }
  }
}
