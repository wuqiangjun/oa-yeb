package com.xxxx.server.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xxxx.server.pojo.EmployeeTrain;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author revelvy
 * @since 2021-08-07
 */
public interface EmployeeTrainMapper extends BaseMapper<EmployeeTrain> {

}
