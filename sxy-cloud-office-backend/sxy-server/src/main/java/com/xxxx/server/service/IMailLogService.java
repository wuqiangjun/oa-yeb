package com.xxxx.server.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xxxx.server.pojo.MailLog;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author revelvy
 * @since 2021-08-07
 */
public interface IMailLogService extends IService<MailLog> {

}
