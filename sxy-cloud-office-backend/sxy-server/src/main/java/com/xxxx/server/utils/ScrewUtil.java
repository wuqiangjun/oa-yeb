package com.xxxx.server.utils;

import cn.smallbun.screw.core.Configuration;
import cn.smallbun.screw.core.engine.EngineConfig;
import cn.smallbun.screw.core.engine.EngineFileType;
import cn.smallbun.screw.core.engine.EngineTemplateType;
import cn.smallbun.screw.core.execute.DocumentationExecute;
import cn.smallbun.screw.core.process.ProcessConfig;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Revelvy
 * @since 1.0
 */
public class ScrewUtil {
    /**
     * 获取数据源
     *
     * @return dataSource
     */
    private static DataSource getDataSource() {
        HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setDriverClassName("com.mysql.cj.jdbc.Driver");
        hikariConfig.setJdbcUrl("jdbc:mysql://localhost:3306/yeb?useSSL=false&useUnicode=true&characterEncoding=utf-8&serverTimezone=GMT%2B8");
        hikariConfig.setUsername("root");
        hikariConfig.setPassword("Delicious.2222");
        hikariConfig.addDataSourceProperty("useInformationSchema", "true");
        hikariConfig.setMinimumIdle(2);
        hikariConfig.setMaximumPoolSize(5);
        return new HikariDataSource(hikariConfig);
    }

    /**
     * 获取文档的生成配置
     *
     * @return engineConfig
     */
    private static EngineConfig getEngineConfig() {
        return EngineConfig.builder()
                //文档的输出路径
                .fileOutputDir(System.getProperty("user.dir") + "/doc")
                //文档生成后是否自动打开目录
                .openOutputDir(true)
                //文档格式（支持HTML、WORD、MD）
                .fileType(EngineFileType.MD)
                //模板实现
                .produceType(EngineTemplateType.freemarker)
                //自定义输出文件名
                .fileName("随心云办公-数据库结构文档")
                .build();
    }

    /**
     * 获取文档的表处理配置
     *
     * @return processConfig
     */
    private static ProcessConfig getProcessConfig() {
        List<String> designatedTableName = new ArrayList<>();
        List<String> designatedTablePrefix = Arrays.asList("t_");
        List<String> designatedTableSuffix = new ArrayList<>();
        List<String> ignoreTableName = Arrays.asList("aa", "test_group");
        List<String> ignoreTablePrefix = Arrays.asList("test_");
        List<String> ignoreTableSuffix = Arrays.asList("_test");
        return ProcessConfig.builder()
                //根据表名生成
                .designatedTableName(designatedTableName)
                //根据表前缀生成
                .designatedTablePrefix(designatedTablePrefix)
                //根据表后缀生成
                .designatedTableSuffix(designatedTableSuffix)
                //忽略表名
                .ignoreTableName(ignoreTableName)
                //忽略表前缀
                .ignoreTablePrefix(ignoreTablePrefix)
                //忽略表后缀
                .ignoreTableSuffix(ignoreTableSuffix)
                .build();
    }

    /**
     * 获取 Screw 完整配置
     *
     * @param dataSource    数据源
     * @param engineConfig  文档的生成配置
     * @param processConfig 文档的表处理配置
     * @return configuration
     */
    private static Configuration getScrewConfig(DataSource dataSource, EngineConfig engineConfig, ProcessConfig processConfig) {
        return Configuration.builder()
                //文档版本
                .version("1.0.0")
                //文档描述
                .description("数据库设计文档生成")
                //数据源
                .dataSource(dataSource)
                //文档的生成配置
                .engineConfig(engineConfig)
                //文档的表处理配置
                .produceConfig(processConfig)
                .build();
    }

    public static void main(String[] args) {
        new DocumentationExecute(getScrewConfig(getDataSource(), getEngineConfig(), getProcessConfig())).execute();
    }
}
