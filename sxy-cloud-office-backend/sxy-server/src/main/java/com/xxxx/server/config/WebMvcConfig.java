package com.xxxx.server.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * Author: Revelvy
 */
@Configuration
public class WebMvcConfig implements WebMvcConfigurer {
    // 跨域
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")  // 允许所有当前域
                .allowedOrigins("*")   // 允许所有外部域
                .allowedHeaders("*")
                .allowedMethods("GET","POST","PUT","DELETE")
                .maxAge(60 * 30);
    }
}
