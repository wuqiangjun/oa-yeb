package com.xxxx.server.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xxxx.server.pojo.PoliticsStatus;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author revelvy
 * @since 2021-08-07
 */
public interface IPoliticsStatusService extends IService<PoliticsStatus> {

}
