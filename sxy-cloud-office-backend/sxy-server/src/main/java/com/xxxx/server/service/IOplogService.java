package com.xxxx.server.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xxxx.server.pojo.Oplog;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author revelvy
 * @since 2021-08-07
 */
public interface IOplogService extends IService<Oplog> {

}
