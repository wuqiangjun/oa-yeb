##  项目简介

利用空闲时间写了一套前后端分离的云办公管理系统，后续有空会陆续更新一些实用功能。

[![License](https://img.shields.io/badge/License-Apache%202.0-green.svg)](https://gitee.com/revelvy/sxy-cloud-office-frontend/blob/master/LICENSE)
[![Language](https://img.shields.io/badge/language-java-red.svg)](https://www.java.com/)
[![Tools](https://img.shields.io/badge/tools-IDEA-blue.svg)](https://www.jetbrains.com/idea/)
[![Platform](https://img.shields.io/badge/platform-linux%20%7C%20macos%20%7C%20windows-lightgrey.svg)](https://img.shields.io/badge/platform-linux%20%7C%20macos20%7C%20windows-lightgrey.svg)

## 技术栈
* CentOS 7.7
* SpringBoot 2.3.0.RELEASE
* JDK 1.8
* MyBatis Plus 3.3.1
* Redis 5.0.12
* MySQL 8.0.18
* RabbitMQ 3.8.5

##  预置功能

1.  用户管理：配置系统用户个人中心、角色、权限以及状态等。
2.  部门管理：配置系统部门的组织机构，树结构展现。
3.  职位管理：配置系统用户所属担任职务。
4.  职称管理：配置系统用户所属担任职务的职称。
5.  角色权限管理：配置不同角色所能操作资源的权限。
6.  薪资管理：配置各部门和所属员工详细薪资分配体系。
7.  人事管理：配置员工入职资料等相关信息，支持搜索、数据导入及其导出，表结构展示。
8.  在线聊天：方便用户在线沟通交流。
9.  操作日志：系统正常操作日志记录和查询，系统异常信息日志记录和查询。
10. 系统接口：根据业务代码自动生成相关的 API 接口文档。
11. 邮件任务：新员工成功登记入职自动完成入职欢迎邮件投递。
12. 参数管理：对系统动态配置常用参数。


## 演示图例
<table>
    <tr>
        <td><img src="https://gitee.com/revelvy/picbed/raw/master/blog/img/20210811115550.png"/></td>
        <td><img src="https://gitee.com/revelvy/picbed/raw/master/blog/img/20210811121230.png"/></td>
    </tr>
    <tr>
        <td><img src="https://gitee.com/revelvy/picbed/raw/master/blog/img/20210811121232.png"/></td>
        <td><img src="https://gitee.com/revelvy/picbed/raw/master/blog/img/20210811125317.png"/></td>
    </tr>
    <tr>
        <td><img src="https://gitee.com/revelvy/picbed/raw/master/blog/img/20210811125257.png"/></td>
        <td><img src="https://gitee.com/revelvy/picbed/raw/master/blog/img/20210811121235.png"/></td>
    </tr>
	<tr>
        <td><img src="https://gitee.com/revelvy/picbed/raw/master/blog/img/20210811121237.png"/></td>
        <td><img src="https://gitee.com/revelvy/picbed/raw/master/blog/img/20210811121239.png"/></td>
    </tr>	 
    <tr>
        <td><img src="https://gitee.com/revelvy/picbed/raw/master/blog/img/20210811121241.png"/></td>
        <td><img src="https://gitee.com/revelvy/picbed/raw/master/blog/img/20210811121243.png"/></td>
    </tr>
	<tr>
        <td><img src="https://gitee.com/revelvy/picbed/raw/master/blog/img/20210811121246.png"/></td>
        <td><img src="https://gitee.com/revelvy/picbed/raw/master/blog/img/20210811121248.png"/></td>
    </tr>
	<tr>
        <td><img src="https://gitee.com/revelvy/picbed/raw/master/blog/img/20210811121252.png"/></td>
        <td><img src="https://gitee.com/revelvy/picbed/raw/master/blog/img/20210811121254.png"/></td>
    </tr>
	<tr>
        <td><img src="https://gitee.com/revelvy/picbed/raw/master/blog/img/20210811121257.png"/></td>
        <td><img src="https://gitee.com/revelvy/picbed/raw/master/blog/img/20210811121259.png"/></td>
    </tr>
</table>

## 我的博客

我的博客：[![白竹竹](https://img.shields.io/badge/白竹竹-green.svg)](http://www.blog.revelvy.cn/) 

