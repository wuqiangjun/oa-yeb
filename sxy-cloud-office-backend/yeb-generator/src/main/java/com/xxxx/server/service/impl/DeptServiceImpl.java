package com.xxxx.server.service.impl;

import com.xxxx.server.pojo.Dept;
import com.xxxx.server.mapper.DeptMapper;
import com.xxxx.server.service.IDeptService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Revelvy
 * @since 2022-01-29
 */
@Service
public class DeptServiceImpl extends ServiceImpl<DeptMapper, Dept> implements IDeptService {

}
