package com.xxxx.server.mapper;

import com.xxxx.server.pojo.Dept;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Revelvy
 * @since 2022-01-29
 */
public interface DeptMapper extends BaseMapper<Dept> {

}
