package com.xxxx.server.service;

import com.xxxx.server.pojo.Dept;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Revelvy
 * @since 2022-01-29
 */
public interface IDeptService extends IService<Dept> {

}
